//
//  main.m
//  Test-iOS
//
//  Created by Satyam Krishna on 26/03/17.
//  Copyright © 2017 Satyam Krishna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
